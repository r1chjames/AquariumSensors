import paho.mqtt.client as mqtt
from AppLogger import logger as app_logger


class MqttClient(object):

    def __init__(self, config, client):
        self._mqtt_server = config['DEFAULT']['mqtt_server']
        self._mqtt_port = int(config['DEFAULT']['mqtt_port'])
        self._mqtt_client = client
        self._mqtt_user = config['DEFAULT']['mqtt_user']
        self._mqtt_password = config['DEFAULT']['mqtt_password']

        self.client = mqtt.Client(self._mqtt_client)
        self.client.on_connect = self.on_connect
        self.client.on_publish = self.on_publish
        self.client.on_disconnect = self.on_disconnect
        self.client.username_pw_set(self._mqtt_user, self._mqtt_password)
        self.client.connect(self._mqtt_server, self._mqtt_port)

    def on_connect(self, client, userdata, flags, rc):
        app_logger.info(f"{self._mqtt_client}: Connected with status: {str(rc)}")

    def on_disconnect(self, client, userdata, rc):
        app_logger.info(f"{self._mqtt_client}: Disconnected from MQTT with status: {str(rc)}. Attempting reconnect...")

    def on_publish(self, client, userdata, mid):
        app_logger.info(f"{self._mqtt_client}: Message published")

    def send_message(self, transaction_id, topic, parameter):
        app_logger.info(f"{self._mqtt_client}: ID: {str(transaction_id)} - Sending MQTT message with content: {parameter} "
                        f"to topic: {topic}")
        return self.client.publish(topic, parameter, qos=1, retain=True)[0]

    def subscribe(self, topic, qos):
        self.client.subscribe(topic, qos)
        app_logger.info(f"{self._mqtt_client}: Subscribed to topic: {topic}")
