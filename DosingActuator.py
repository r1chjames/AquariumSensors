import datetime
import json
import sys
import traceback
import uuid
from threading import Thread
from time import sleep
import RPi.GPIO as GPIO
from MqttClient import MqttClient
from AppLogger import logger as app_logger


class DosingActuator(Thread):

    def __init__(self, config):
        self._config = config
        self._actuation_topic = config['DEFAULT']['actuation_topic']
        self._dosing_confirmation_topic = config['DEFAULT']['dosing_confirmation_topic']
        self._keepalive_topic = config['DEFAULT']['keepalive_topic']
        self._client = config['DEFAULT']['mqtt_client'] + "_dosing"
        self._keepalive_interval = int(config['DEFAULT']['keepalive_interval'])

        self._mqtt_client = MqttClient(config, self._client)
        self._mqtt_client.subscribe(self._actuation_topic, 2)
        self._mqtt_client.client.on_message = self.on_message
        self._mqtt_client.client.loop_start()
        Thread.__init__(self)
        self.daemon = True

    def on_message(self, client, userdata, msg):
        transaction_id = uuid.uuid4()
        app_logger.info(f"ID: {str(transaction_id)} - Message received: {str(msg.payload)}")
        self.actuate_pump(msg, transaction_id)

    def actuate_pump(self, msg, transaction_id):
        payload = json.loads(msg.payload)
        pump = str(payload['pump'])
        seconds = payload['seconds']
        try:
            dosing_bcm_gpio = int(self._config['DEFAULT']['dosing_bcm_gpio_' + pump])
            app_logger.debug(f"ID: {str(transaction_id)} - Using BCM GPIO: {dosing_bcm_gpio}")
            app_logger.info(f"ID: {str(transaction_id)} - Actuating pump: {pump} for {seconds} seconds")

            GPIO.setmode(GPIO.BCM)
            GPIO.setwarnings(False)
            GPIO.setup(dosing_bcm_gpio, GPIO.OUT)
            GPIO.output(dosing_bcm_gpio, GPIO.LOW)
            sleep(seconds)
            GPIO.output(dosing_bcm_gpio, GPIO.HIGH)
            app_logger.info(f"ID: {str(transaction_id)} - Finished dosing")
            self.publish_confirmation(pump, transaction_id)
            GPIO.cleanup()
        except KeyboardInterrupt:
            GPIO.cleanup()
        except Exception as e:
            app_logger.error(e)
            traceback.print_tb(sys.exc_info()[2])
            raise


    def publish_confirmation(self, pump, transaction_id):
        confirmation_topic = self._dosing_confirmation_topic + pump
        rc = self._mqtt_client.send_message(transaction_id, confirmation_topic, get_date_time())
        app_logger.debug(f"ID: {str(transaction_id)} - Confirmation status: {str(rc)}")

    def run(self):
        try:
            while True:
                # self.send_keepalive_message()
                sleep(self._keepalive_interval)
        except KeyboardInterrupt:
            self._mqtt_client.client.loop_stop()
        except Exception as e:
            app_logger.error(e)
            traceback.print_tb(sys.exc_info()[2])
            raise

def add_string_placeholder(str_to_eval, client):
    return eval(f'f"""{str_to_eval}"""')

def get_date_time():
    return str(datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S"))
