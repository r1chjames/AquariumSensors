import sys
import json
import traceback
from time import sleep
from threading import Thread

from AppLogger import logger as app_logger
from MqttClient import MqttClient
from ReadFailedException import ReadFailedException
from lib import mh_z19

previous_co2 = "0"


def read_co2():
    try:
        return mh_z19.read('/dev/ttyUSB0')
    except Exception as e:
        app_logger.info(f"Exceptiom class: {e.__class__.__name__}")
        if e.__class__.__name__ == 'TypeError':
            app_logger.error(f"MH-Z19 not detected: {e}")
        if e.__class__.__name__ == 'PermissionError' or e.__class__.__name__ == 'serial.serialutil.SerialException':
            app_logger.error(f"MH-Z19 read failed - access denied: {e}")
        traceback.print_tb(sys.exc_info()[2])
        raise ReadFailedException(e)


class Co2Monitor(Thread):

    def __init__(self, config):
        self._config = config
        self.co2_topic = config['DEFAULT']['co2_topic']
        client = config['DEFAULT']['mqtt_client'] + "_co2"

        self._mqtt_client = MqttClient(config, client)
        self._mqtt_client.client.loop_start()
        Thread.__init__(self)
        self.daemon = True

    def process_co2(self):
        new_co2 = json.loads(read_co2()).co2
        global previous_co2
        if new_co2 != previous_co2:
            rc = self._mqtt_client.send_message(self.co2_topic, new_co2)
            previous_co2 = new_co2
            app_logger.debug(f"CO2 Status: {str(rc)}")
        else:
            app_logger.debug("CO2 unchanged")

    def run(self):
        try:
            while True:
                app_logger.debug("Beginning CO2 sensor loop")
                self.process_co2()
                sleep(30)
                app_logger.debug("Ended CO2 sensor loop")
        except KeyboardInterrupt:
            self._mqtt_client.client.loop_stop()
        except Exception as e:
            app_logger.error(f"CO2 sensor error: {e}")
            traceback.print_tb(sys.exc_info()[2])
            raise